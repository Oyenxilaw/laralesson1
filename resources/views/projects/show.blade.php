@extends('layouts.app')
@section('content')

    @extends('layouts.message')

<br>


    <h1 class="h1">
        
        Project
    
    </h1><br>

    <div class="box">

        <div class="row">

            <div class="col-md-4 col-sm-4">

                <h1 class="h1">
                    
                    {{$project->title}}
                
                </h1>

            </div>

            <div class="col-md-8 col-sm-8">

                <h3 class="h3">
                    
                    {{$project->description}}
                
                </h3>
                
                <br><br>
                <hr>

                <small class="small">
                    
                    {{$project->created_at}}
                
                </small>

            </div>

        </div>

        <div class="row">
            
            <div class="col-md-1 col-sm-1">
                
                <button class="button is-medium is-outlined is-rounded is-link is-primary">
                    
                    <a href="/projects/{{$project->id}}/edit">
                        
                        EDIT
                    
                    </a>
                
                </button>

            </div><br>

            <div class="col-md-1 col-sm-1">

                <form action="/projects/{{$project->id}}" method="post">

                    @csrf

                    @method("DELETE")

                    <button class="button is-medium is-outlined is-rounded is-danger is-link">DELETE</button>

                </form>

            </div>
            
            
        </div>

    </div>

    <br>

    <div class="row">

        <div class="col-md-4 col-sm-4">

            @if(count($project->tasks) > 0)
                <div class="box">
                    <h1 class="h1">Tasks</h1><hr>
                    @foreach ($project->tasks as $task)
                        <form action="/completed-task/{{$task->id}}" method="post">
                            {{-- @csrf --}}
                            {{csrf_field()}}
                            @if ($task->completed)
                                @method("DELETE")
                            @endif
                        
                            
                            <label for="tasks" class="{{$task->completed ? 'is-complete' : ''}}">
                                <input type="checkbox" name="completed" class="checkbox" onchange="this.form.submit()" {{$task->completed ? 'checked' : ''}}>
                                {{$task->description}}
                            </label>
            
                        </form>
                    @endforeach
                </div> 
            
            @endif
            <div class="box"></div>
        </div>
        <div class="col-md-8 col-sm-8">
            <div class="box">
                <h1 class="h1">Add New Task</h1><hr>
                
                <form action="/tasks/{{$project->id}}" method="POST">
                
                    @csrf
                    <br>
                    <label class="label" for="description">Description</label><br>
                    <input type="text" name="description" class="input is-rounded" required><br><br>
                    <button class="button is-medium is-outlined is-rounded is-link is-primary">Create Task</button>


                </form>
                
            </div>
            <div class="row">
                <div class="col-md-6"><div class="box animated2 shake"></div></div>
                    <div class="col-md-6"><div class="box animated2 shake"></div></div>
                    
            </div>
            
        </div>
    </div><br><br><br>
   
    
@endsection