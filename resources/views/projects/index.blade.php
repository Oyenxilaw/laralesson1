@extends('layouts.app')

@section('content')

<br>
<br>
    <h1 class="h1">
        
        Projects
    
    </h1>
    <hr>

    <div class="box">

        @if (count($projects) > 0)

            @foreach ($projects as $project)

            <div class="box">

                <h3 class="h3">
                    
                    <a href="/projects/{{$project->id}}">
                    
                        {{$project->title}}
                
                    </a>
                
                </h3>

            </div>
                
            @endforeach

            {{$projects->links()}}
            

        @else

            <h2 class="h2">
                
                No project defined yet!!
            
            </h2>

        @endif 

        <br>

        <button class="button is-rounded is-medium is-outlined is-link is-secondary ">
            
            <a href="/projects/create">
                
                Create New Project
            
            </a>
        
        </button>

    </div>

    <br><br>
    
@endsection