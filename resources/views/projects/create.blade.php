@extends('layouts.app')

@section('content')

    @extends('layouts.message')

<br>
    <h1 class="h1">Create New Project</h1><br>

    <div class="box">

        <form action="/projects" method="post">

            @csrf

            <label for="title" class="label">Title</label>

            <input type="text" name="title" class="input {{$errors ? 'is-danger' : '' }} " required>

            <label for="description" class="label">Desription</label>

            <textarea name="description" class="textarea {{$errors ? 'is-danger' : '' }}" required></textarea>

            <br>
            <button class="button is-medium is-danger is-rounded">

                Create Project

            </button>

        </form>

    </div>

    <div class="row">

        <div class="col-md-6 col-sm-6"><div class="box"></div></div>
        <div class="col-md-4 col-sm-4"><div class="box"></div></div>
        <div class="col-md-2 col-sm-2"><div class="box"></div></div>

    </div>

    <br><br>

@endsection