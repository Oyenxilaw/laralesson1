@extends('layouts.app')

@section('content')

    @extends('layouts.message')

<br>

    <h1 class="h1">
        
        Update Project
    
    </h1>
    <br>

    <div class="box">

        <form action="/projects/{{$project->id}}" method="POST">

            @csrf

            @method("PATCH")

            <label for="title" class="label">
                
                Title
            
            </label>
            <input type="text" name="title" class="input {{$errors ? 'is-danger' : '' }}" value="{{$project->title}}" required >
            
            <br><br>
            <label for="description" class="label">
                
                Desription
            
            </label>
            <textarea name="description" class="textarea {{$errors ? 'is-danger' : '' }}" required>
                
                {{$project->description}}
            
            </textarea>

            <br>

            <button class="button is-outlined is-medium is-primary is-rounded">

                Create Project

            </button>

        </form>

    </div>
    
@endsection