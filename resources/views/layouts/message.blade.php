@if (count($errors) > 0)

    @foreach ($errors->all() as $error)

                <div class="notification is-danger">
                    <button class="delete"></button><br>
                <div class="row" style="margin-left:40px;text-align:center;">
                    <img src="{{asset('img/warning.png')}}" alt="warning">
                    <h3 class="h5">{{$error}}</h3>
                </div>
                  
                </div>
            
    @endforeach

    <br><br>

@endif

@if (session('error'))

    <div class="notification is-danger">
        
        <button class="delete"></button>

        <h3 class="h5">{{session('error')}}</h3>

    </div>

@endif

@if (session('success'))

    <div class="notification is-success">

        <button class="delete"></button>

        <h3 class="h5">{{session('success')}}</h3>

    </div>

@endif