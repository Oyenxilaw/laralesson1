<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded=[];

    public function project(){

        return $this->belongsTo(Project::class);

    }

    public function complete($complete){


       return  $this->update(
            ['completed' => $complete]
        );

        

    }
    public function Incomplete($complete){

        return $this->update([
            'completed'=> $complete
        ]);

    }
}
