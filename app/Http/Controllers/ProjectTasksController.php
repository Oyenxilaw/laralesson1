<?php

namespace App\Http\Controllers;
use App\Task;
use App\Project;

use Illuminate\Http\Request;

class ProjectTasksController extends Controller
{

    public function store(Task $task, Project $project){

        request()->validate([

            'description' => ['required','min:3']

        ]);
        
        $task->create([

            'description' => request('description'),
        
            'project_id' => $project->id

        ]);

        return back()->with('success','New Task Created Successfully');


    }
    
}
