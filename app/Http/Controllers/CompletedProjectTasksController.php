<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class CompletedProjectTasksController extends Controller
{
    public function store(Task $task){
        
        $task->complete(true);
        return back();


    }
    public function destroy(Task $task){

        $task->Incomplete(false);
        return back();

    }
}
